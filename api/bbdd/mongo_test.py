import pymongo
import urllib
import datetime

def conn():
    password = '@jVZfv-uFEs23Ff'
    myclient = pymongo.MongoClient(f'mongodb+srv://infodemic:{urllib.parse.quote(password)}@cluster0.8kvp6.mongodb.net/infodemic-DB?retryWrites=true&w=majority')
    mydb = myclient['infodemic-DB']
    return mydb


def insertTrain(collection,accuracy,error):
    db = conn()
    mycoll = db[f'{collection}_train_logs']
    date = datetime.datetime.now()
    mydict = {'timestamp': date, 'accuracy': accuracy, 'error': error}
    mycoll.insert_one(mydict)

def insertRequest(collection, user, tweet, result, error):
    db = conn()
    mycoll = db[f'{collection}_request_logs']
    date = datetime.datetime.now()
    mydict = {'timestamp': date, 'user': user, 'tweet': tweet, 'result': result, 'error': error}
    mycoll.insert_one(mydict)