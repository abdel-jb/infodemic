import pandas as pd
import numpy as np
from sklearn.feature_extraction.text import CountVectorizer
from sklearn.naive_bayes import MultinomialNB
import pickle
# from algorithms.utils.modify_text import transform_text
from sklearn.metrics import accuracy_score
import os

cwd = os.getcwd()

model_file = 'finalized_model.sav'
vectorizer_file = 'vectorizer_model.sav'
path_model = cwd + '/algorithms/data/'
BASE_FOLDER=  cwd + '/algorithms/data/'

def train():

    #import data
    with open(BASE_FOLDER + 'train.csv', "rb") as input_file:
        df_train = pd.read_csv(input_file)
    with open(BASE_FOLDER + 'test.csv', "rb") as input_file:
        df_test = pd.read_csv(input_file)
    with open(BASE_FOLDER + 'val.csv', "rb") as input_file:
        df_val = pd.read_csv(input_file)

    df_train_max = pd.concat([df_train, df_val])

    #transform text
    # df_t = transform_text(df_t)

    #train
    vectorizer = CountVectorizer()
    counts = vectorizer.fit_transform(df_train_max['tweet'].values)
    classifier = MultinomialNB(alpha = 0.2)
    targets = df_train_max['label'].values
    classifier.fit(counts, targets)
    example_counts = vectorizer.transform(df_test['tweet'])
    predictions = classifier.predict(example_counts)
    accuracy = accuracy_score(df_test['label'], predictions)

    #save the model
    pickle.dump(classifier, open(path_model + model_file, 'wb'))
    pickle.dump(vectorizer, open(path_model + vectorizer_file, 'wb'))

    return accuracy