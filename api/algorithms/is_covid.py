import pandas as pd
import numpy as np
import random
from sklearn.feature_extraction.text import CountVectorizer
from sklearn.naive_bayes import MultinomialNB
import pickle
#from algorithms.utils.modify_text import transform_text
from sklearn.model_selection import train_test_split
from sklearn.metrics import accuracy_score
import os

cwd = os.getcwd()
model_file = 'finalized_iscovid.sav'
vectorizer_file = 'vectorizer_iscovid.sav'
path_model = cwd + '/algorithms/data/'
BASE_FOLDER= cwd + '/algorithms/data/'

def train():

    #import data
    with open(BASE_FOLDER + 'train.csv', "rb") as input_file:
        df_train = pd.read_csv(input_file)
    with open(BASE_FOLDER + 'test.csv', "rb") as input_file:
        df_test = pd.read_csv(input_file)
    with open(BASE_FOLDER + 'val.csv', "rb") as input_file:
        df_val = pd.read_csv(input_file)
    header_list = ['1', '2', '3', '4', '5', 'tweet']
    with open(BASE_FOLDER +'random.csv', 'rb') as input_file:
        df_random = pd.read_csv(input_file,  encoding='latin-1', names = header_list)

    df_total = pd.DataFrame(df_random['tweet'])
    df_total['label'] = 0
    df_total = df_total.iloc[1:df_total.shape[0]//4,]

    train = pd.DataFrame(df_train['tweet'])
    validation = pd.DataFrame(df_val['tweet'])

    test = pd.DataFrame(df_test['tweet'])
    df_covid = pd.concat([train, validation, test])

    df_covid['label'] = 1
    df_t = pd.concat([df_covid, df_total])

    #transform text
    # df_t = transform_text(df_t)

    df_t = df_t.reset_index(drop = True)
    df_t.sample(frac=1).reset_index(drop = True)

    #split data
    train, test = train_test_split(df_t, test_size=0.2)

    #train
    vectorizer = CountVectorizer()
    counts = vectorizer.fit_transform(train['tweet'].values)
    classifier = MultinomialNB(alpha = 0.2)
    targets = train['label'].values
    classifier.fit(counts, targets)
    example_counts = vectorizer.transform(test['tweet'])
    predictions = classifier.predict(example_counts)
    accuracy = accuracy_score(test['label'], predictions)

    #save the model
    pickle.dump(classifier, open(path_model + model_file, 'wb'))
    pickle.dump(vectorizer, open(path_model + vectorizer_file, 'wb'))

    return accuracy