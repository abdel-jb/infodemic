from textblob import TextBlob
import pickle
import logging

# un comentario
def iserror(tweet):
    try:
        tweet.translate(to='en')
        return False
    except:
        return True


def translate(string):
    try:
        blob = TextBlob(string)
        if blob.detect_language() != 'en':
            if iserror(blob) == False:
                return str(blob.translate(to='en'))
        return string
    except Exception as e:
        logging.warning(e)
        return string

def transform_text(df):
    df['tweet'] = df['tweet'].str.lower()
    a = 'http'
    df['link'] = df['tweet'].str.contains('https', regex=False)
    df_subset = df['tweet'][df['link'] == True]
    df['tweet'][df['link'] == True] = df_subset.apply(lambda x: x[0: x.find('https'):] + x[(x.find('https')+23)::])
    return df


def check_tweet(tweet, option):
    try:
        model_file = f'finalized_{option}.sav'
        vectorizer_file = f'vectorizer_{option}.sav'
        path_model = './algorithms/data/'
        # load the model from disk
        loaded_model = pickle.load(open(path_model + model_file, 'rb'))
        loaded_vectorizer = pickle.load(open(path_model + vectorizer_file, 'rb'))
        res = loaded_model.predict(loaded_vectorizer.transform([tweet]))

        if res[0] == 'real' or res[0] == 1:
            return True
        elif res[0] == 'fake' or res[0] == 0:
            return False
    except Exception as e:
        logging.warning(e)