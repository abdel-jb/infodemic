from flask import Flask, request, jsonify
# from functools import wraps
# import jwt
import datetime
from algorithms import model,is_covid
from flask_cors import CORS, cross_origin
from algorithms.utils.modify_text import translate, check_tweet
from bbdd import mongo_test
from concurrent.futures import ThreadPoolExecutor
import logging
import logging.config

logging.config.fileConfig('logging.conf')


def create_app(testing: bool = True):
    app = Flask(__name__)
    app.config['SECRET_KEY'] = 'cmV0cmFzYWRvYWJkZWw='
    cors = CORS(app, resources={r"/*": {"origins": "*"}})

    model.train()
    is_covid.train()

    def addRecordToDatabase(data):
        # loop over data
        try:
            for d in data:
                mongo_test.insertRequest(*d)
        except Exception as e:
            logging.warning(e)

    @app.route('/checkText', methods=['POST'])
    @cross_origin()
    def checkText():
        data = request.json
        text = translate(data['text']) if data['translate'] else data['text']
        author = data['author']
        tmp_data = []
        returnResponse = {}
        if len(text) > 3:
            if check_tweet(text, 'iscovid'):
                tmp_data.append(['check_covid', author, text, True, False])
                if check_tweet(text, 'model'):
                    tmp_data.append(['check_fake_news', author, text, True, False])
                    returnResponse = {'tweet': True}
                else:
                    tmp_data.append(['check_fake_news', author, text, False, False])
                    returnResponse = {'tweet': False}
            else:
                tmp_data.append(['check_covid', author, text, False, False])
                returnResponse = {'iscovid': False}
        with ThreadPoolExecutor(max_workers=1) as executor:
            executor.submit(addRecordToDatabase, tmp_data)
        return {'ok': False, 'message': 'Length must be greater than 3'} if returnResponse == {} else returnResponse

    # def check_for_token(func):
    #     @wraps(func)
    #     def wrapped(*args, **kwargs ):
    #         try:
    #             token = request.headers.get('Authorization').split(" ")[1]
    #             if not token:
    #                 return jsonify({'success': False, 'error': 'Missing token header.'}), 403
    #             data = jwt.decode(token, app.config['SECRET_KEY'], algorithms=["HS256"])
    #         except Exception as error:
    #             return jsonify({'success': False, 'error': str(error)}),403
    #         return func(*args, **kwargs)
    #     return wrapped

    # @app.route('/checkToken', methods=['POST'])
    # @check_for_token
    # def checkToken():
    #     return jsonify({'success': True, 'msg': 'Token is valid.'})

    # @app.route('/data', methods=['GET'])
    # @check_for_token
    # def authorised():
    #     return jsonify({'success': True, 'msg': 'This is only viewable with a token'})

    # @app.route('/login', methods=['POST'])
    # def login():
    #     # get data from request
    #     data = request.get_json()
    #     # TODO: check if username and password match
    #     if data.get('username', False) == 'password@test.com': # Consultar user y passwd en la db
    #         token  = jwt.encode({
    #             'user': 'prueba',
    #             'exp': datetime.datetime.utcnow() + datetime.timedelta(weeks=1)
    #         },
    #         app.config['SECRET_KEY'],
    #         algorithm="HS256"
    #         )
    #         return jsonify({'success': True, 'token': token})
    #     else:
    #         return jsonify({
    #             'success': False,
    #             'error': 'Username or Password do not match... BOT.'
    #         })


    # @app.route('/train', methods=['POST'])
    # @cross_origin()
    # def trainModels():
    #     errorModelTrain = False
    #     error_isCovid_Train = False
    #     accuracyModel = None
    #     accuracyIsCovid = None

    #     try:
    #         accuracyModel = model.train()
    #     except:
    #         errorModelTrain = True

    #     try:
    #         accuracyIsCovid = is_covid.train()
    #     except:
    #         error_isCovid_Train = True

    #     mongo_test.insertTrain('check_fake_news', accuracyModel, errorModelTrain)
    #     mongo_test.insertTrain('check_covid', accuracyIsCovid, error_isCovid_Train)

    #     return True

    return app
