#!/bin/bash

echo "Deploying content to raspberry..."

DEPLOYPATH=/home/abdel/projects/infodemic/ 

echo "Copying files..."
docker-compose down
cp -rp * $DEPLOYPATH || { echo "Failed during copy, please check"; exit 2; }

echo "CDing to ${DEPLOYPATH}..."
cd $DEPLOYPATH || exit 4

echo "Building image if necessary..."
docker image ls | grep -qri "infodemic" \
    && echo "Already exists, last compiled at:" $(docker image inspect infodemic_api:latest | grep -i created | sed -Er "s/.*Created\": \"(.*)\..*/\1/" | xargs) \
    || docker-compose build api

echo "Starting docker containers in case not started..."
docker-compose up -d || { echo "Failed during docker-compose up -d, please check..."; exit 5; }